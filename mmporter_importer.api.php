<?php

/**
 * @file
 * Documentation of hooks invoked by the mmporter_exporter module
 *
 * This file is here for documentation purposes only. It does not get included
 * in the running code.
 */

/**
 * @addtogroup nodeporter_hooks
 * @{
 */

/**
 * Answer an array of user-info arrays based on a source XML element.
 *
 * The array should be of the form:
 *    array(
 *      '123' => array(     // 123 is the uid
 *        'uid'   => 123,
 *        'name'  => 'example_user',
 *        'mail'  => 'example@example.com',
 *      ),
 *    )
 *
 * @param DOMElement $userElement
 * @return array
 */
function hook_np_get_matching_users(DOMElement $user_element) {

}

/**
 * Answer an array of group-info arrays based on a source XML element.
 *
 * The array should be of the form:
 *    array(
 *      '123' => array(     // 123 is the group id
 *        'id'   => 123,
 *        'name'  => 'example_group',
 *      ),
 *    )
 *
 * @param DOMElement $groupElement
 * @return array
 */
function hook_np_get_matching_groups(DOMElement $group_element) {

}

/**
 * @} End of "addtogroup nodeporter_hooks".
 */
