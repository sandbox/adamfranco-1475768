<?php

/**
 * @file
 * Documentation of hooks invoked by the mmporter_exporter module
 *
 * This file is here for documentation purposes only. It does not get included
 * in the running code.
 */

/**
 * @addtogroup nodeporter_hooks
 * @{
 */

/**
 * Add attributes about a user.
 *
 * @param int $uid
 * @param string $name
 * @return array
 */
function hook_np_get_user_attras($uid) {

}

/**
 * Add attributes about a group.
 *
 * Implementation of hook, np_get_group_attras($attras)
 *
 * @param string $id
 * @return array
 */
function hook_np_get_group_attras($id) {

}

/**
 * @} End of "addtogroup nodeporter_hooks".
 */
