<?php

/**
 * @file
 * Common functions used by the Monster Menus Porter module.
 */

/**
 * Common functions for dealing with Monster Menus permissions.
 *
 *
 * @copyright Copyright &copy; 2010, Middlebury College
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License (GPL)
 */
class MMPerms {

  /**
   * Answer the groups with permissions.
   *
   * Returns a 2-dimensional array of the form:
   *  array(
   *    'r' => array(380 => 'Test Group'),
   *    'w' => array(),
   *    'a' => array(),
   *    'u' => array(),
   *  )
   *
   * @param int $mmtid
   * @return array
   */
  public static function getGroupPerms($mmtid) {
    $grouplist = array('r' => array(), 'w' => array(), 'a' => array(), 'u' => array());
    $result = db_query('SELECT t2.mmtid, t2.name, a.mode FROM {mm_tree} t ' .
      'INNER JOIN {mm_tree_access} a ON a.mmtid = t.mmtid ' .
      'LEFT JOIN {mm_tree} t2 ON a.gid = t2.mmtid ' .
      'WHERE t2.mmtid >= 0 AND a.mmtid = %d ORDER BY t2.name', $mmtid);
    while ($r = db_fetch_object($result)) {
      if (mm_content_user_can($r->mmtid, 'u'))
        $grouplist[$r->mode][$r->mmtid] = mm_content_expand_name($r->name);
    }

    return $grouplist;
  }

  /**
   * Answer the users with permissions.
   *
   * Returns a 2-dimensional array of the form:
   *  array(
   *    'r' => array(),
   *    'w' => array(),
   *    'a' => array(5 => 'username'),
   *    'u' => array(),
   *  )
   *
   * @param int $mmtid
   * @return array
   */
  public static function getUserPerms($mmtid) {
    $userlist = array('r' => array(), 'w' => array(), 'a' => array(), 'u' => array());
    $result = db_query('SELECT a.mode, a.gid FROM {mm_tree} t ' .
      'INNER JOIN {mm_tree_access} a ON a.mmtid = t.mmtid ' .
      'WHERE a.gid < 0 AND a.mmtid = %d', $mmtid);
    while ($r = db_fetch_object($result)) {
      $users = mm_content_users_in_group($r->gid, NULL, TRUE, MM_UI_MAX_USERS_IN_GROUP);
      if (!is_null($users))
        $userlist[$r->mode] = $users;
    }

    return $userlist;
  }

  /**
   * Answer the permissions granted to everyone.
   *
   * Will return a one-dimensional array with zero or more of: 'r', 'w', 'a', 'u'
   *
   * @param int $mmtid
   * @return array()
   */
  public static function getDefaultPerms($mmtid) {
    $default_mode = db_result(db_query('SELECT default_mode FROM {mm_tree} mmtid = %d', $mmtid));
    return explode(',', $default_mode);
  }
}
